const express = require('express')
var cors = require('cors')
const app = express()
app.use(cors())

const bodyParser = require('body-parser')
const port = 3000

// create application/json parser
var jsonParser = bodyParser.json()

app.get('/', (req, res) => {
  res.send('Hello World!')
});

let students = [];
const user = {
    username: "user",
    password: "password123"    
}

app.post('/login', jsonParser, (req, res) => {
    const { username, password } = req.body;
    if (username === user.username && password === user.password) {
        res.send('ok')
    } else {
        res.send(401)
    }
});

app.post('/students', jsonParser, (req, res) => {
    const {name, email, course} = req.body;
    if (name && email && course) {
        students.push({name, email, course})
        res.send('Student has been added')
    } else {
        res.sendStatus(400)
    }
});

app.delete('/students/:email', jsonParser, (req, res) => {
    const { email } = req.params;
    students = students.filter(x => x.email !== email)
    res.sendStatus(200)
});

app.get('/students', (req, res) => {
    res.send(students)
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});